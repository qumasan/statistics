# Statistics

統計の学習ノート

## 参考図書

1. 「データ・誤差解析の基礎」林茂雄・馬場凉訳、東京化学同人（2013）

## ノート（mystmd）の起動

```console
$ cd statistics    # このリポジトリ
$ source .venv/bin/activate.fish    # Fish用のスクリプト
(statistics) $ cd mystmd
(statistics) $ myst start
```

執筆環境（Python環境）はRyeを使って管理しています。
プロジェクト直下にある``.venv/bin/activate``を使って仮想環境を有効にします。
シェルごとにスクリプトが用意されいるので、自分のシェル環境にあったものを使ってください。