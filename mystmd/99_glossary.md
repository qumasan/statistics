# 統計学の用語

統計学の教科書を読んだり、
SciPyのモジュールなどを眺めたりするときに、
知っておくと良さそうな統計学用語の英訳ををリストしてみました。

:::{glossary}

標本
: sample

平均値
: average

平均
: mean

偏差
: deviation

    :::{math}
    :label: math-deviation
    \Delta x_{i} = x_{i} - \overline{x}
    :::

分散
: variance

    :::{math}
    :label: math-variance
    \begin{align}
    S^{2}
    & = \frac{1}{n} \sum_{k=1}^{n} (\Delta x_{k})^{2} \\
    & = \frac{1}{n} \sum_{k=1}^{n} \left( x_{i} - \overline{x} \right)^{2}
    \end{align}
    :::

標準偏差
: standard deviation

    :::{math}
    :label: math-standard-deviation
    \begin{align}
    s 
    & = \sqrt{S^{2}}\\
    & = \sqrt{ \frac{1}{n} \sum_{k=1}^{n} \left( x_{i} - \overline{x} \right)^{2}}

    \end{align}
    :::

変動係数
: coefficient of variation

    :::{math}
    :label: math-coefficient-of-variation
    \text{CV} = \frac{ s (\text{std. dev.})}{ \overline{x} (\text{mean})}
    :::

相関係数
: coefficient of correlation（ピアソンの相関係数）

    :::{math}
    :label: math-coefficient-of-correlation
    \begin{align}
    r
    & = \frac{
            (x_{1} - \overline{x}) (y_{1} - \overline{y}) +
            (x_{2} - \overline{x}) (y_{2} - \overline{y}) +
            (x_{3} - \overline{x}) (y_{3} - \overline{y}) +
            \cdots +
            (x_{n} - \overline{x}) (y_{n} - \overline{y})
        }{
            \sqrt{
                (x_{1} - \overline{x})^{2} +
                (x_{2} - \overline{x})^{2} +
                \cdots + 
                (x_{n} - \overline{x})^{2}
                }
            \sqrt{
                (y_{1} - \overline{y})^{2} +
                (y_{2} - \overline{y})^{2} +
                \cdots + 
                (y_{n} - \overline{y})^{2}
                }                
        } \\
    & = \frac{
            \Delta x_{1} \Delta y_{1} +
            \Delta x_{2} \Delta y_{2} +
            \Delta x_{3} \Delta y_{3} +
            \cdots +
            \Delta x_{n} \Delta y_{n}

        }{
            \sqrt{
                (\Delta x_{1})^{2} +
                (\Delta x_{2})^{2} +
                \cdots + 
                (\Delta x_{n})^{2}
                }
            \sqrt{
                (\Delta y_{1})^{2} +
                (\Delta y_{2})^{2} +
                \cdots + 
                (\Delta y_{n})^{2} +
                }                
        } \\        
    & = \frac{
            \prod_{k=1}^{n} \Delta x_{k} \Delta y_{k}
        }{
            \sqrt{ \sum (\Delta x_{k})^{2} }
            \sqrt{ \sum (\Delta y_{k})^{2} }                
        }        
        \end{align}
    :::

母集団
: population

母平均
: population mean

母分散
: population variance

誤差
: error

統計誤差
: statistic error

系統誤差
: systematic error

確率変数
: random variables

離散型確率変数
: discrete random variable

連続型確率変数
: continuous random variable

確率分布
: probability distribution

確率質量関数
: probability mass function

確率密度関数
: probability density function

累積分布関数
: cumulative probability function

二項分布
: binominal distribution

ガウス分布
: Gauss distribution

正規分布
: normal distribution

ポアソン分布
: poisson distribution

検定
: test

カイ二乗検定
: chi-squared test

自由度
: degree of freedom

帰無仮説
: null hypothesis

信頼区間
: confidence interval

記述統計学
: descriptive statistics

推測統計学
: inductive statistics

ベイズ統計学
: Bayesian statistics


:::